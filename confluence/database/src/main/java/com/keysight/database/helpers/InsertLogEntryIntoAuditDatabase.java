package com.keysight.database.helpers;

import com.keysight.database.helpers.DatabaseQueryHelper;
import com.keysight.database.helpers.ConnectionProfile;
import com.keysight.database.helpers.PluginConfigManager;

import java.sql.*;
import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.lang3.StringUtils;

public class InsertLogEntryIntoAuditDatabase implements Runnable {

    ConnectionProfile auditLogProfile;
    PluginConfigManager pluginConfigManager;
    ConnectionProfile profile;
    String fullName;
    String userName;
    String pageUrl;
    String pageName;
    String spaceName;
    int rowCount;
    int queryDuration;
    String sql;

    Connection connect;
    PreparedStatement preparedStatement;
    PreparedStatement trimPreparedStatement;

    private static final Logger log = LoggerFactory.getLogger(InsertLogEntryIntoAuditDatabase.class);

    public InsertLogEntryIntoAuditDatabase( ConnectionProfile auditLogProfile,
                                            PluginConfigManager pluginConfigManager,
                                            ConnectionProfile profile,
                                            String fullName,
                                            String userName,
                                            String pageUrl,
                                            String pageName,
                                            String spaceName,
                                            int rowCount,
                                            int queryDuration,
                                            String sql) {
        this.auditLogProfile = auditLogProfile;
        this.pluginConfigManager = pluginConfigManager;
        this.profile = profile;
        this.auditLogProfile = auditLogProfile;
        this.fullName = fullName;
        this.userName = userName;
        this.pageUrl = pageUrl;
        this.pageName = pageName;
        this.spaceName = spaceName;
        this.rowCount = rowCount;
        this.queryDuration = queryDuration;
        this.sql = sql;
    }

    public void run() {
        String logEntryTableName = pluginConfigManager.getLogEntryTableName();
        String logEntryLifeTime  = pluginConfigManager.getLogEntryLifeTime();
        double daysBack = -1.0;


        String logSql = "INSERT INTO " + logEntryTableName + "("
                      + "fullName, userName,"
                      + "pageUrl, pageName, spaceName,"
                      + "databaseProfile, rowCount, queryDuration, sqlQuery, timestamp)"
                      + " VALUES "
                      + "(?,?,?,?,?,?,?,?,?,?)";

        String trimSql = "DELETE FROM " + logEntryTableName + " "
                       + "WHERE timestamp <= ?";

        try
        {
            connect = DatabaseQueryHelper.createConnection(auditLogProfile, pluginConfigManager);
            preparedStatement = connect.prepareStatement(logSql);
            preparedStatement.setString(1, fullName);
            preparedStatement.setString(2, userName);
            preparedStatement.setString(3, pageUrl);
            preparedStatement.setString(4, pageName);
            preparedStatement.setString(5, spaceName);
            preparedStatement.setString(6, profile.getName());
            preparedStatement.setInt(7, rowCount);
            preparedStatement.setInt(8, queryDuration);
            preparedStatement.setString(9, sql);
            preparedStatement.setObject(10, LocalDateTime.now());

            preparedStatement.executeUpdate();
            preparedStatement.close();

            if( !StringUtils.isEmpty( logEntryLifeTime )){
                try {
                    daysBack = Float.parseFloat( logEntryLifeTime );
                }
                catch ( Exception floatConversionException )
                {
                    log.error("DB Connection error trying to convert the Log Entry Life Time number " + logEntryLifeTime + " to a float.\n" );
                }

                if( daysBack > 0 )
                {
                    LocalDateTime dateThreshold = LocalDateTime.now();
                    try
                    {
                        trimPreparedStatement = connect.prepareStatement(trimSql);
                        trimPreparedStatement.setObject(1, dateThreshold.minusMinutes( (long) (daysBack * 24 * 60) ) );
                        trimPreparedStatement.executeUpdate();
                    }
                    catch ( Exception trimException )
                    {
                        log.error("DB Connection error trying to trim the Log Entry table: " + trimException.getMessage() + "\n" );
                    }
                }
            }
        }
        catch( Exception e)
        {
            log.error("DB Connection error trying to update the audit log: " + e.getMessage() + "\n");
        } finally {
            close();
        }
    }

    private void close() {
        try {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (trimPreparedStatement != null) {
                trimPreparedStatement.close();
            }
            if (connect != null) {
                connect.close();
            }
        } catch (Exception e) {
        }
    }

}