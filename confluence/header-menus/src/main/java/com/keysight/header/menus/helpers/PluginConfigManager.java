package com.keysight.header.menus.helpers;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.codec.binary.Base64;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLDecoder;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Driver;
import java.sql.DriverManager;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

@Component
public class PluginConfigManager {
    // note, may need to load jdb2jcc.jar and db2jcc_license_cu.jar

    private static final Logger log = LoggerFactory.getLogger(PluginConfigManager.class);

    private Element configRoot;
    private String configXml;

    private final PluginSettingsFactory pluginSettingsFactory;
    private final TransactionTemplate transactionTemplate;

    @Autowired
    public PluginConfigManager(final PluginSettingsFactory pluginSettingsFactory,
                               final TransactionTemplate transactionTemplate) {
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.transactionTemplate = transactionTemplate;
        this.loadFromStorage();
    }

    public void loadFromStorage()
    {
        DocumentBuilder builder;
        Document document;
        try {
            this.loadConfigXmlFromStorage();
            builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            document = builder.parse(new InputSource(new StringReader(configXml)));
            configRoot = document.getDocumentElement();

            //String jTdsDriverUrl = new String(Base64.decodeBase64(getValueOfElement("jtds-driver-url")), "UTF-8");
            //if (!StringUtils.isEmpty(jTdsDriverUrl)){
                //JTDS_SQL_SERVER_DOWNLOAD = jTdsDriverUrl;
            //}

        } catch (Exception exception) {
            log.warn("Unable to load the DB Connector plugin configuration:" + exception);
        }
    }

    private void loadConfigXmlFromStorage()
    {
        List<String> menuLetters = HeaderMenusConstants.getMenuLetters();
        List<String> menuFields  = HeaderMenusConstants.getMenuFields();

        String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                   + "<plugin-configuration>\n";
        for( String menuLetter : menuLetters ){
            xml += "   <menu-" + menuLetter + ">\n";
            for( String menuField : menuFields ) {
                xml += "   <" + menuField + "></" + menuField + ">\n";
            }
            xml += "   </menu-" + menuLetter + ">\n";
        }
        xml += "</plugin-configuration>\n";

        try {
            PluginConfigContainer pluginConfigContainer = (PluginConfigContainer) transactionTemplate.execute((TransactionCallback<Object>) () -> {
                PluginSettings settings = pluginSettingsFactory.createGlobalSettings();
                PluginConfigContainer innerPluginConfigContainer = new PluginConfigContainer();
                innerPluginConfigContainer.setXml((String) settings.get(PluginConfigManager.class.getName() + ".xml"));
                return innerPluginConfigContainer;
            });

            if (pluginConfigContainer.getXml() != null && !StringUtils.isEmpty( pluginConfigContainer.getXml() )) {
                xml = pluginConfigContainer.getXml();
            }
        }
        catch( Exception exception )
        {
            log.warn( "Failed to retrieve or parse the profile xml config: " + exception.getMessage() );
        }

        this.configXml = xml;
    }

    public boolean setConfigXml( String xml )
    {
        boolean updated = false;
        if( this.configXmlIsValid( xml ) ) {
            updated = true;
            transactionTemplate.execute(() -> {
                PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
                pluginSettings.put(PluginConfigManager.class.getName() + ".xml", xml);
                return null;
            });
            this.loadFromStorage();
        }
        return updated;
    }

    public String getConfigXml()
    {
        return configXml;
    }

    private boolean configXmlIsValid( String xml )
    {
        boolean flag = true;
        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document = builder.parse(new InputSource(new StringReader(xml)));
            Element root = document.getDocumentElement();
        }
        catch( Exception exception )
        {
            flag = false;
        }
        return flag;
    }

    public Menu getMenu( String menuLetter )
    {
        if( this.configRoot != null ) {
            NodeList menuElement = configRoot.getElementsByTagName("menu-"+menuLetter);
            if( menuElement.getLength() >= 0 ){
                return new Menu( (Element) menuElement.item(0) );
            } else {
                return null;
            }
        } else {
            log.warn( "No configRoot: Probably failed to parse the xml config file.");
            return null;
        }
    }

    private String getValueOfElement(String tagName) {
        NodeList allNodes = configRoot.getElementsByTagName(tagName);
        String text = null;
        for (int i = 0; i < allNodes.getLength(); i++) {
            if (allNodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
                text = allNodes.item(i).getTextContent();
                break;
            }
        }
        return text;
    }
}


