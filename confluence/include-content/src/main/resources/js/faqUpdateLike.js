(function (jQuery) {
    jQuery(document).ready(function() {
       bindLikeThisControl();
    });
})(jQuery);

function bindLikeThisControl(){
   // form the URL
   var url = AJS.contextPath() + "/rest/include-content/1.0/faq/toggle-like";

   jQuery(".keysight-faq-like-control").unbind( "click" );
   jQuery(".keysight-faq-like-control").click(function(e) {
      e.preventDefault();

      // request the config information from the server
      jQuery.ajax({
          url: url,
          type: "GET",
          dataType: "json",
          contentType: "application/x-www-form-urlencoded",
          data: {"PAGE_ID":jQuery(e.target).parent("a").attr("pageId")}
      }).done(function(data) { // when the configuration is returned...
         if( data.userLikePreference === "liked" ){
             jQuery(e.target).html( "(Unlike)" );
             jQuery(e.target).closest("tr.keysight-faq-list-row").find(".keysight-faq-like-description").html( "You like this." );
         } else {
             jQuery(e.target).html( "Like" );
             jQuery(e.target).closest("div").find(".keysight-faq-like-description").html( "" );
         }
          jQuery(e.target).closest("tr.keysight-faq-list-row").find("td.faq-like-count").html( data.likeCount );
      }).fail(function(self,status,error){
          console.error( error );
      });
   });
   return;
}
